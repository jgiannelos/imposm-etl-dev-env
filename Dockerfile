FROM docker-registry.wikimedia.org/buster:latest

RUN apt-get update && apt-get install -y python3-maps-deduped-tilelist python3-swiftclient
RUN groupadd maps && useradd --no-log-init -m -r -g maps maps-dev
ADD files/event-template.json /etc/imposm/event-template.json

USER maps-dev
WORKDIR /home/maps-dev
RUN mkdir workspace
COPY --chown=maps-dev:maps files/osm_expire workspace/osm_expire
COPY --chown=maps-dev:maps files/osm workspace/osm
