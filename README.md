# Setup

```bash
# Prepare docker images
docker build . -t tile-invalidation-dev:latest
docker pull dockerswiftaio/docker-swift:latest

# Create a docker network
docker network create maps-dev

# Run containers
docker run --name swift-storage --network maps-dev -d -v $(pwd):/workspace dockerswiftaio/docker-swift
docker run --name tile-invalidation-workspace -it --network maps-dev -v $(pwd):/workspace tile-invalidation-dev:latest bash

# Example swift commands
# Swift account status
swift -A http://swift-storage:8080/auth/v1.0 -U test:tester -K testing stat

# Upload a dummy file
echo "Example file" > /tmp/example.txt
swift -A http://swift-storage:8080/auth/v1.0 -U test:tester -K testing upload dev-container /tmp/example.txt

# List swift container files
swift -A http://swift-storage:8080/auth/v1.0 -U test:tester -K testing list dev-container
```
